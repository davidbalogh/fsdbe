const {Datastore} = require('@google-cloud/datastore');

const datastore = new Datastore({
    projectId: 'fsdbe-334616',
    keyFilename: 'datastore-credentials.json'
});

const kindName = 'schedule-entity';

exports.create = (req, res) => {
    res.set('Access-Control-Allow-Origin', '*');
    let start_date = req.query.start_date || req.body.start_date;
    let end_date = req.query.end_date || req.body.end_date;

    if (start_date && end_date) {
        datastore
            .save({
                key: datastore.key(kindName),
                data: {
                    start_date: start_date,
                    end_date: end_date
                }
            })
            .catch(err => {
                res.status(500).send('ERROR:', err);
            });
        res.status(200).send("Data transmitted!");
    } res.status(500).send('start_date or end_date is missing');
};

exports.getSchedule = async (req, res) => {
    res.set('Access-Control-Allow-Origin', '*');
    datastore
        .runQuery(datastore.createQuery(kindName))
        .then((schedules) => {
            res.status(200).send(JSON.stringify(schedules[0]));
        })
        .catch((err) => {
            res.status(500).send('ERROR:', err);
        });
};