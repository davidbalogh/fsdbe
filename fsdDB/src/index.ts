import 'reflect-metadata';
import { createConnection } from 'typeorm';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import { Request, Response } from 'express';
import { Routes } from './routes';
import { MnistData } from './tensor/data.js';
import * as  expressFile from 'express-fileupload';
import { getModel, train, doPrediction } from './tensor/script.js';
import { toArrayBuffer } from './tensor/fnToArrayBuffer';
import * as cors from "cors";

const tf = require('@tensorflow/tfjs');
require('@tensorflow/tfjs-node');


createConnection()
	.then(async (connection) => {
		// create express app
		const app = express();
		app.use(bodyParser.json());
		app.use(expressFile());
		app.use(cors());

		const router = express.Router();

		// register express routes from defined application routes
		Routes.forEach((route) => {
			//if present, register middlewares in the order they were given
			if ( route.middleware ) {
				(router as any)[route.method](route.route, route.middleware);
			}
			(router as any)[route.method](
				route.route,
				(req: Request, res: Response, next: Function) => {
					const result = new (route.controller as any)()[route.action](
						req,
						res,
						next,
					);
					if ( result instanceof Promise ) {
						result.then((result) =>
							result !== null && result !== undefined
								? res.json(result)
								: undefined,
						);
					} else if ( result !== null && result !== undefined ) {
						res.json(result);
					}
				},
			);
		});

		app.use('/', router);

		const result = require('dotenv').config();

		if ( result.error ) {
			throw result.error;
		}

		// setup express app here
		// ...

		// start express server

		app.post('/train', async (request, response) => {
			const data = new MnistData();
			const model = getModel();
			await data.load();

			const result = await train(model, data);
			await model.save('file://./src/tensor/model');
		});

		app.post('/evaluate', async (request, response) => {
			const bufferedArray = toArrayBuffer(request.files.image.data);
			const model = await tf.loadLayersModel('file://./src/tensor/model/model.json');
			const predictionResult = await doPrediction(bufferedArray, model);
			response.status(200).send(predictionResult);
		});

		app.listen(3001);

		console.log(
			'Express server has started on port 3001. Open http://localhost:3001/users to see results',
		);
	})
	.catch((error) => console.log(error));