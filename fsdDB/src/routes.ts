import { UserController } from './controller/UserController';
import { authenticateJWT } from './middleware/validate';
import { TensorflowController } from './controller/TensorflowController';

export const Routes = [{
	method: 'get',
	route: '/users',
	controller: UserController,
	action: 'all',
	middleware: authenticateJWT,
}, {
	method: 'get',
	route: '/users/:id',
	controller: UserController,
	action: 'one',
	middleware: authenticateJWT,
}, {
	method: 'post',
	route: '/users',
	controller: UserController,
	action: 'save',
	middleware: authenticateJWT,
}, {
	method: 'delete',
	route: '/users/:id',
	controller: UserController,
	action: 'remove',
	middleware: authenticateJWT,
}, {
	method: 'post',
	route: '/register',
	controller: UserController,
	action: 'register',
}, {
	method: 'post',
	route: '/login',
	controller: UserController,
	action: 'login',
}/*, {
	method: 'post',
	route: '/evaluate',
	controller: TensorflowController,
	action: 'evaluate',
}, {
	method: 'get',
	route: '/train',
	controller: TensorflowController,
	action: 'train',
}*/];