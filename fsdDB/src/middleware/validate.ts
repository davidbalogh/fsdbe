import { NextFunction, Request, Response } from "express";

let jwt = require("jsonwebtoken");

export const authenticateJWT = (req: Request, res: Response, next: NextFunction) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, process.env.SECRET_KEY, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }

            req.user = user;
            return next();
        });
    } else {
        res.sendStatus(401);
    }
};