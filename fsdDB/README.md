# Awesome Project Build with TypeORM

Steps to run this project:

1. Run `npm i` command
2. Setup database settings inside `ormconfig.json` file
3. Run `npm start` command

Google Cloud endpoints:

create: **https://us-central1-fsdbe-334616.cloudfunctions.net/create**

getSchedule: https://us-central1-fsdbe-334616.cloudfunctions.net/getSchedule